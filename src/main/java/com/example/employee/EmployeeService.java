package com.example.employee;

import org.springframework.stereotype.Service;

import java.io.*;
import java.util.Scanner;

@Service
public class EmployeeService {

    File myFile = new File("employeeData.txt");

    public String writeData(Employee employee) throws IOException {
        if (!myFile.exists()) {
            myFile.createNewFile();
        }
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(myFile, true));
        bufferedWriter.append("\n\nName : " + employee.getName() +
                "\nAge: " + employee.getAge() +
                "\nGender :" + employee.getGender() +
                "\nCity : " + employee.getCity());
        bufferedWriter.close();
        return "Successfully wrote data";
    }
    public StringBuffer readData(){
        Scanner scanner;
        StringBuffer stringBuffer = new StringBuffer();
        try{
            scanner = new Scanner(myFile);
        }catch(FileNotFoundException ex){
            return new StringBuffer("File do not exist");
        }

        while (scanner.hasNextLine())
            stringBuffer.append(scanner.nextLine() + "\n");

        if (stringBuffer.length()==0)
            return new StringBuffer("no data found");
        return stringBuffer;
    }


}
