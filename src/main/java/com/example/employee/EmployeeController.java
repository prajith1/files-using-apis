package com.example.employee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.util.Scanner;

@RestController
@RequestMapping("/employee")
public class EmployeeController{

    @Autowired
    private EmployeeService employeeService;

    @PostMapping(value="/data", produces = "application/json")
    public ResponseEntity<String> writeFile(@RequestBody Employee employee) throws IOException {
      String result = employeeService.writeData(employee);
        return ResponseEntity.ok(result);
    }

    @GetMapping(value="/data", produces = "application/json")
    public ResponseEntity<StringBuffer> readFile(){
        StringBuffer stringBuffer = employeeService.readData();
        return ResponseEntity.ok(stringBuffer);
    }
}
